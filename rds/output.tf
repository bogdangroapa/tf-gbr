# Outputs
output "ec2_public_ip" {
  value = aws_instance.my_instance.public_ip
}

output "ec2_private_ip" {
  value = aws_instance.my_instance.private_ip
}

output "rds_instance_ip" {
  value = aws_db_instance.my_rds.address
}

output "rds_random_password" {
  value = random_password.my_password.result
  sensitive = true
}

output "public_ssh_key" {
  value = tls_private_key.my_key.public_key_openssh
}

output "private_ssh_key" {
  value = tls_private_key.my_key.private_key_openssh
  sensitive = true
}