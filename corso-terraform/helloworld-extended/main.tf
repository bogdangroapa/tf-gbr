provider "aws" {
    region = "eu-central-1"
}

resource "aws_instance" "awslab" {
    ami = data.aws_ami.ubuntu.id
    instance_type = var.instance_type
    key_name = aws_key_pair.deployer.id
    vpc_security_group_ids = [aws_security_group.ssh.id , aws_security_group.mysql_sg.id]
    user_data_replace_on_change = true
    user_data = <<-EOF
                #!/bin/bash
                sudo apt install -y apache2
                sudo apt install -y mysql-server
                
                sudo systemctl start apache2
                sudo systemctl start mysql

                EOF
    tags = {
            Name = "Multi Cloud EC2"
  }
}