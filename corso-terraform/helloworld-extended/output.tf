output "public_ip" {
        value = aws_instance.awslab.public_ip
        description = "The public IP address of the web server"
}

output "ssh_key" {
	value = data.local_file.ssh_key_file.content
}
