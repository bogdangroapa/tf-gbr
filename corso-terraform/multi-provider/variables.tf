variable "aws-region" {
    description = "AWS Default region ID"
    default = "eu-central-1"
}

variable "gcp-project_id" {
    description = "GCP Project ID"
    default = "corsoterraform"
}

variable "gcp-region" {
    description = "GCP Default region ID"
    default = "europe-west3"
}

variable "gcp-zone" {
    description = "GCP Default zone ID"
    default = "europe-west3-a"
}
