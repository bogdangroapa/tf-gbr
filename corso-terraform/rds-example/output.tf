output "db_password" {
	value = data.aws_secretsmanager_secret_version.password.secret_string
	sensitive = true
}

output "address" {
	value = aws_db_instance.default.address
	description = "Connect to the database at this endpoint"
}

output "port" {
	value = aws_db_instance.default.port
	description = "The port database is listening on"
}

# output "public_ip" {
# 	value = { for i in aws_instance.example: i.id => "${i.public_ip}" }
# #	value = aws_instance.example[*].public_ip
# 	description = "The public IP address of the web server"
# }