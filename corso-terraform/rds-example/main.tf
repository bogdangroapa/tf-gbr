provider "aws" {
    region = "eu-central-1"
}

# Create the secretmanager
resource "aws_secretsmanager_secret" "rds-admin" {
  name = "rdssecret"
  recovery_window_in_days = 0
}

# Create the random password
resource "random_password" "password" {
  length           = 16
  special          = true
}

# Add the password to the secret
resource "aws_secretsmanager_secret_version" "rds-admin" {
  secret_id     = aws_secretsmanager_secret.rds-admin.id
  secret_string = random_password.password.result
}

# Retrieve info from the Secret Manager secret
data "aws_secretsmanager_secret" "password" {
  #name = "rdsadmin"
  name = aws_secretsmanager_secret.rds-admin.id
  depends_on = [aws_secretsmanager_secret.rds-admin]
}

# Retrieve info from the Secret Manager secret version (value included)
data "aws_secretsmanager_secret_version" "password" {
  secret_id = data.aws_secretsmanager_secret.password.id
}

resource "aws_security_group" "db" {
    name = "rds"
    ingress {
		from_port = 3306
		to_port = 3306
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
}

resource "aws_db_instance" "default" {
  identifier            = "testdb"
  allocated_storage    = 10
  engine               = "mariadb"
  instance_class       = "db.t3.micro"
  db_name              = "mydb"
  username             = "dbadmin"
  password             = data.aws_secretsmanager_secret_version.password.secret_string
  skip_final_snapshot  = true
  publicly_accessible  = false
  vpc_security_group_ids = [aws_security_group.db.id]
}
