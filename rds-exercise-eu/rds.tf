# RDS instance
resource "aws_db_instance" "my_rds" {
  allocated_storage   = 10
  storage_type        = "gp2"
  engine              = "mariadb"
  instance_class      = "db.t3.micro"
  username            = "admin"
  password            = random_password.my_password.result
  skip_final_snapshot = true
  vpc_security_group_ids = [aws_security_group.rds_sg.id]

}

# Generate random password
resource "random_password" "my_password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

