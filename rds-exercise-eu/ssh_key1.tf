resource "tls_private_key" "my_key" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "local_sensitive_file" "private_key" {
  filename        = pathexpand("~/.ssh/gbr-ec2-key")
  content         = tls_private_key.my_key.private_key_pem
  file_permission = "0400"
}

resource "local_sensitive_file" "public_key" {
  filename        = pathexpand("~/.ssh/gbr-ec2-key.pub")
  content         = tls_private_key.my_key.public_key_pem
  file_permission = "0400"
}