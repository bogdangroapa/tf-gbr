provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "helloworld" {
  ami           = "ami-023adaba598e661ac"
  instance_type = "t2.micro"
  vpc_security_group_ids = [ aws_security_group.fw-rules.id , aws_security_group.web-rules.id ]
  key_name = aws_key_pair.helloworld-example.id
  tags = {
    Name   = "HelloWorld GBR"
    Course = "TF Fundamentals"
  }
  lifecycle {
    prevent_destroy = false
  }
}

resource "aws_key_pair" "helloworld-example" {
  key_name   = "tf-ssh"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJKa6m5NHEnTZ2dpN96GobQJ5kzMxF40dlT4dhOYDEA8 student@groapa-9grz"
}

resource "aws_security_group" "fw-rules" {
        name = "terraform-example-instance"
        ingress {
                from_port = 22
                to_port = 22
                protocol = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }
}

resource "aws_security_group" "web-rules" {
        name = "terraform-example-web"
        ingress {
                from_port = 80
                to_port = 80
                protocol = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }
}