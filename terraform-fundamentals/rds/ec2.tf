# EC@ instance
resource "aws_instance" "my_instance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.ssh_sg.id]
  user_data     = <<-EOT
    #!/bin/bash
    apt-get update
    apt-get install -y apache2 mysql-client
    sudo a2enmod ssl
    echo "My Public IP is $(curl -s ifconfig.me)" > /var/www/html/index.html
    systemctl restart apache2
  EOT
  tags = {
    Name = "GBR instance exercises"
  }
}