provider "local" {

}

variable "stringtest" {
  type    = string
  default = "string"
}

variable "listtest" {
  type = list(string)
  default = [ "4","tt","ciao"]
}